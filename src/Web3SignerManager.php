<?php

namespace Drupal\web3_signer;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\web3_signer\Annotation\Web3Signer as Web3SignerAnnotation;
use Drupal\web3_signer\PluginForm\Web3SignerFormBase;
use Drupal\web3_signer\Web3SignerManagerInterface;
use Drupal\web3_signer\Web3SignerPluginInterface;
use \Traversable as Traversable;

/**
 * Provides a Web3Signer plugin manager.
 *
 * @see plugin_api
 */
class Web3SignerManager extends DefaultPluginManager implements Web3SignerManagerInterface {

  /**
   * {@inheritDoc}
   */
  public function __construct(Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Web3Signer',
      $namespaces,
      $module_handler,
      Web3SignerPluginInterface::class,
      Web3SignerAnnotation::class
    );
    $this->alterInfo('web3_signer_info');
    $this->setCacheBackend($cache_backend, 'web3_signer_info_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $definitions = parent::getDefinitions();
    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginIds() {
    return array_keys($this->getDefinitions());
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'private_key';
  }

  /**
   * {@inheritDoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    // Register default plugin configuration form.
    if (!($definition['forms']['edit'] ?? NULL)) {
      $definition['forms']['edit'] = Web3SignerFormBase::class;
    }
  }
}
