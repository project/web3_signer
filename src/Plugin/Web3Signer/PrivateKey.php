<?php

namespace Drupal\web3_signer\Plugin\Web3Signer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\web3_signer\Plugin\Web3Signer\Web3SignerBase;
use Drupal\web3_signer\Transaction;

/**
 * Defines the web3_signer plugin to communicate with Ganache blockchain.
 *
 * @Web3Signer(
 *   id = "private_key",
 *   title = @Translation("Private key"),
 *   description = @Translation("Do not use on production!")
 * )
 */
class PrivateKey extends Web3SignerBase {

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array &$form, FormStateInterface $form_state) {
    $form['private_key'] = [
      '#type' => 'password',
      '#title' => $this->t('Private key'),
      '#description' => $this->t('Will not be saved.') . ' ' . $this->t('Paste the hexadecimal value, not your seed phrase.') . ' ' .
        $this->t('Do not use this form if you do not trust this website.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @todo Implements private key format validation.
    $private_key = $form_state->getValue('private_key');
    if (!$private_key || empty($private_key)) {
      $form_state->setErrorByName('private_key', $this->t('Wrong or empty private key'));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function sign(array $values): string {
    $private_key = $values['private_key'] ?? '';

    $args = [];
    $args['from'] = $values['from'] ?? NULL;
    $args['to'] = $values['to'] ?? NULL;
    $args['value'] = $values['value'] ?? NULL;
    $args['nonce'] = $values['nonce'] ?? NULL;
    $args['gas'] = $values['gasLimit'] ?? NULL;
    $args['gasPrice'] = 6000000000; // 6 gwei
    $args['chainId'] = hexdec($values['chainId'] ?? '0x0');

    $transaction = new Transaction($args);
    $signed_tx =  '0x' . $transaction->sign($private_key);

    return $signed_tx;
  }
}
