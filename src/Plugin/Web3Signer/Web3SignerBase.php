<?php

namespace Drupal\web3_signer\Plugin\Web3Signer;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\web3_signer\Web3SignerPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Web3Signer plugins.
 */
abstract class Web3SignerBase extends PluginBase implements Web3SignerPluginInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;
  use PluginWithFormsTrait;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.channel.web3_signer');
    $instance->eventDispatcher = $container->get('event_dispatcher');

    return $instance;
  }


  /*************************************************************************
   * Overridable form methods
   * @see \Drupal\web3_signer\PluginForm\Web3SignerFormBase
   *************************************************************************/

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array &$form, FormStateInterface $form_state) {
    $form['nothing'] = [
      '#markup' => $this->t('Implement <em>@function</em> in web3_signer plugin: @id', [
        '@function' => __FUNCTION__,
        '@id' => $this->getPluginId(),
      ])
    ];

    return $form;
  }


  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Nothing specific to validate by default.
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Sign and keep value for later use in parent form.
    $signed_tx = $this->sign($form_state->getValues());
    $form_state->setValue('signed_tx', $signed_tx);
  }
}
