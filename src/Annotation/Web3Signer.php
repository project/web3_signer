<?php

namespace Drupal\web3_signer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a web3_signer annotation object.
 *
 * Plugin Namespace: Plugin\Web3Signer
 *
 * @see plugin_api
 *
 * @Annotation
 */
class Web3Signer extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

  /**
   * List of custom plugin forms.
   *
   * @see hook_web3_signer_info_alter();
   * @see \Drupal\Core\Plugin\PluginWithFormTrait;
   * @see \Drupal\web3_signer\PluginForm\Web3SignerFormBase;
   *
   * @var array
   */
  public $forms;
}
