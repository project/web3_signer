<?php

namespace Drupal\web3_signer;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;

/**
 * Defines the common interface for all Web3Signer manager classes.
 *
 * @see \Drupal\web3_signer\Web3SignerManager
 * @see plugin_api
 */
interface Web3SignerManagerInterface extends FallbackPluginManagerInterface {
}
