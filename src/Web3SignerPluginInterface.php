<?php

namespace Drupal\web3_signer;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;

/**
 * Defines the common interface for all Web3Signer plugin classes.
 *
 * @see \Drupal\web3_signer\Web3SignerManager
 * @see \Drupal\web3_signer\Annotation\Web3Signer
 * @see plugin_api
 */
interface Web3SignerPluginInterface extends PluginInspectionInterface, PluginWithFormsInterface {

  /**
   * Sign a transaction
   *
   * @param array $values
   *   A given list of values.
   *
   * @return string
   *   The signed transaction.
   */
  public function sign(array $values): string;
}
