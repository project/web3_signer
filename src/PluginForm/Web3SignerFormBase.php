<?php

namespace Drupal\web3_signer\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormBase;

/**
 * A base form class for Web3Signer plugin configuration.
 */
class Web3SignerFormBase extends PluginFormBase {

  /**
   * Return the plugin instance.
   *
   * @return \Drupal\web3_signer\Web3SignerPluginInterface
   */
  public function getPlugin() {
    return $this->plugin;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    $form_id = [];
    $form_id[] = 'web3_signer';
    if ($plugin_id = $this->getPlugin()->getPluginId() ?? NULL) {
      $form_id[] = 'plugin';
      $form_id[] = $plugin_id;
    }
    $form_id[] = 'form';

    return implode('_', $form_id);
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->plugin->buildConfigurationForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->plugin->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->plugin->submitConfigurationForm($form, $form_state);
  }
}
