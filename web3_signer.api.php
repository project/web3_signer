<?php

/**
 * @file
 * Contains information about hooks defined in this module.
 */

/**
 * Edit existing Web3Signer definitions.
 *
 * @param array $web3_signers
 *   List of web3_signers, passed by reference
 */
function hook_web3_signer_info_alter(array &$definitions) {
  // Do your things here.
}
